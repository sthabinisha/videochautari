package haamroapp.videochautari.com;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import haamroapp.videochautari.com.util.NetworkUtil;


public class NoInternetActivity extends Activity {

	public static final String INTERNET_UNAVAILABLE = "INTERNET_UNAVAILABLE";
	public static final String INTERNET_UNAVAILABLE_MESSAGE = "INTERNET_UNAVAILABLE_MESSAGE";

	private static final String TAG = "NoInternetActivity";

	private TextView textView;
	private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_no_internet);

		textView = (TextView) findViewById(R.id.messageText);
		progressBar = (ProgressBar) findViewById(R.id.progressBarConnecting);

		int message = getIntent().getIntExtra(
				NoInternetActivity.INTERNET_UNAVAILABLE_MESSAGE,
				NetworkUtil.NETWORK_STATE_UNKNOWN_MESSAGE);

		textView.setText(getResources().getString(message));
	}

	public void loadSettings(View view) {

		Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
		startActivity(intent);
	}

	public void doRetry(View view) {
		progressBar.setVisibility(View.VISIBLE);
		textView.setText("Trying to re-connect...");

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				if (NetworkUtil.isOnline(NoInternetActivity.this)) {
					Intent intent = new Intent(NoInternetActivity.this, SplashScreen.class);

					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_CLEAR_TASK
							| Intent.FLAG_ACTIVITY_NEW_TASK);

					startActivity(intent);
				} else {

					progressBar.setVisibility(View.INVISIBLE);
					textView.setText(getResources().getString(
							NetworkUtil.NETWORK_NOT_AVAILABLE_MESSAGE));

				}

			}

		}, 2500);
	}
}
