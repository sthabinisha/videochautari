package haamroapp.videochautari.com;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;

import haamroapp.videochautari.com.adapter.myAdapter;
import haamroapp.videochautari.com.parser.AllParser;
import haamroapp.videochautari.com.parser.XMLParser;

public class MobileVideoChautari extends AppCompatActivity {
    Toolbar mtoolbar;
    DrawerLayout mDrawer;
    ActionBarDrawerToggle mDrawerToggle;
    ImageView image;
    RecyclerView recyclerView;
    RecyclerView.Adapter madapter;
    String TITLES[];
    String dataCategory;
    AllParser parser;
//    static final String URL = "http://my.videochautari.com/categories";
//    static String headerTx = "Recent Videos";
//    static final String KEY_CATEGORY_ITEM = "category";
//    static final String KEY_CATEGORY_LEAF_ITEM = "categoryLeaf";
//    static final String KEY_CATEGORY_TITLE = "title";
//    static final String KEY_CATEGORY_DESCRIPTION = "description";
//    static final String KEY_FEED_URL = "feed";
//    static final String KEY_CATEGORY_THUMB_URL = "sd_img";
//    static final String KEY_FEED_ITEM = "item";
//    static final String KEY_FEED_TITLE = "title";
//    static final String KEY_FEED_THUMB_URL = "sdImg";
//    static final String KEY_FEED_SYNOPSIS = "synopsis";
//    static final String KEY_FEED_VIEWS = "views";
//    static final String KEY_FEED_DURATION = "videoLength";
//    HashMap<String, String> mapc, mapc1, mapf;
//    ArrayList<HashMap<String, String>> categoryList, categoryList1, feedList;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_video_chautari);
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        image  = (ImageView) findViewById(R.id.circleView);
        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);

        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawer,mtoolbar,R.string.opendrawer,R.string.closedrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        parser= new AllParser();
        parser.categoryFeed();


        recyclerView.setLayoutManager(new LinearLayoutManager(MobileVideoChautari.this));
        final GestureDetector mGestureDetector = new GestureDetector(MobileVideoChautari.this, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });
    }



//
//
//
//        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(mtoolbar);
//        mDrawer = (DrawerLayout) findViewById(R.id.DrawerLayout);

//        categoryFeed();
//        TITLES=new String[]{KEY_CATEGORY_TITLE};
//
//        madapter = new myAdapter(TITLES);
//        recyclerView.setLayoutManager(new LinearLayoutManager(MobileVideoChautari.this));
//        final GestureDetector mGestureDetector = new GestureDetector(MobileVideoChautari.this, new GestureDetector.SimpleOnGestureListener() {
//
//            @Override
//            public boolean onSingleTapUp(MotionEvent e) {
//                return true;
//            }
//
//        });
//
//        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawer,mtoolbar,R.string.opendrawer,R.string.closedrawer){
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//            }
//        };
//        mDrawer.setDrawerListener(mDrawerToggle);
//        mDrawerToggle.syncState();
//    }
//
//
//    public void categoryFeed() {
//        categoryList = new ArrayList<HashMap<String, String>>();
//        categoryList1 = new ArrayList<HashMap<String, String>>();
//        XMLParser parser = new XMLParser();
//        String xml = parser.getXmlFromUrl(URL);
//
//        Document doc = parser.getDomElement(xml);
//
//        NodeList nl = doc.getElementsByTagName(KEY_CATEGORY_ITEM);
//        System.out.println(xml + "nl");
//        NodeList nl1 = doc.getElementsByTagName(KEY_CATEGORY_LEAF_ITEM);
////		Log.d("doc", nl+"");
//        for (int i = 0; i < nl.getLength(); i++) {
//            mapc = new HashMap<String, String>();
//            Element e = (Element) nl.item(i);
//            mapc.put(KEY_CATEGORY_TITLE, e.getAttribute(KEY_CATEGORY_TITLE));
//            mapc.put(KEY_CATEGORY_DESCRIPTION,
//                    e.getAttribute(KEY_CATEGORY_DESCRIPTION));
//            mapc.put(KEY_CATEGORY_THUMB_URL,
//                    e.getAttribute(KEY_CATEGORY_THUMB_URL));
//            for (int j = 0; j < nl1.getLength(); j++) {
//                mapc1 = new HashMap<String, String>();
//                Element e1 = (Element) nl1.item(j);
//                mapc1.put(KEY_FEED_URL, e1.getAttribute("feed"));
//                categoryList1.add(mapc1);
//
//            }
//
//            categoryList.add(mapc);
//            Log.d("TAG", categoryList.toString() + "" );
//        }
//        Log.d("Tag", categoryList1.toString() + "category value");
//        Log.d("TAG", categoryList1.size() + "category size");
//
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_mobile_video_chautari, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
