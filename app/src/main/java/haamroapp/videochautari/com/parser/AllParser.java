package haamroapp.videochautari.com.parser;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by binisha on 10/9/15.
 */
public class AllParser {
    static final String URL = "http://my.videochautari.com/categories";
    static String headerTx = "Recent Videos";
    static final String KEY_CATEGORY_ITEM = "category";
    static final String KEY_CATEGORY_LEAF_ITEM = "categoryLeaf";
    static final String KEY_CATEGORY_TITLE = "title";
    static final String KEY_CATEGORY_DESCRIPTION = "description";
    static final String KEY_FEED_URL = "feed";
    static final String KEY_CATEGORY_THUMB_URL = "sd_img";
    static final String KEY_FEED_ITEM = "item";
    static final String KEY_FEED_TITLE = "title";
    static final String KEY_FEED_THUMB_URL = "sdImg";
    static final String KEY_FEED_SYNOPSIS = "synopsis";
    static final String KEY_FEED_VIEWS = "views";
    static final String KEY_FEED_DURATION = "videoLength";
    HashMap<String, String> mapc, mapc1, mapf;
    ArrayList<HashMap<String, String>> categoryList, categoryList1, feedList;


    public void categoryFeed() {
        categoryList = new ArrayList<HashMap<String, String>>();
        categoryList1 = new ArrayList<HashMap<String, String>>();
        XMLParser parser = new XMLParser();
        String xml = parser.getXmlFromUrl(URL);

        Document doc = parser.getDomElement(xml);

        NodeList nl = doc.getElementsByTagName(KEY_CATEGORY_ITEM);
        System.out.println(xml + "nl");
        NodeList nl1 = doc.getElementsByTagName(KEY_CATEGORY_LEAF_ITEM);
//		Log.d("doc", nl+"");
        for (int i = 0; i < nl.getLength(); i++) {
            mapc = new HashMap<String, String>();
            Element e = (Element) nl.item(i);
            mapc.put(KEY_CATEGORY_TITLE, e.getAttribute(KEY_CATEGORY_TITLE));
            mapc.put(KEY_CATEGORY_DESCRIPTION,
                    e.getAttribute(KEY_CATEGORY_DESCRIPTION));
            mapc.put(KEY_CATEGORY_THUMB_URL,
                    e.getAttribute(KEY_CATEGORY_THUMB_URL));
            for (int j = 0; j < nl1.getLength(); j++) {
                mapc1 = new HashMap<String, String>();
                Element e1 = (Element) nl1.item(j);
                mapc1.put(KEY_FEED_URL, e1.getAttribute("feed"));
                categoryList1.add(mapc1);

            }

            categoryList.add(mapc);
            Log.d("TAG", categoryList.toString() + "");
        }
        Log.d("Tag", categoryList1.toString() + "category value");
        Log.d("TAG", categoryList1.size() + "category size");

    }
}
