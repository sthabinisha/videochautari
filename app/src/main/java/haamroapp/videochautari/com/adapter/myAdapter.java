package haamroapp.videochautari.com.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import haamroapp.videochautari.com.R;


/**
 * Created by Dell on 13-Aug-15.
 */
public class myAdapter extends
        RecyclerView.Adapter<myAdapter.ViewHolder> {

    String mTitle[];
    int mIcons[];


    public myAdapter(String titles[]) {
        this.mTitle = titles;
    }

    @Override
    public myAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.row_item, parent, false);

        ViewHolder vhItem = new ViewHolder(itemView,viewType); //Creating ViewHolder and passing the object of type view

        return vhItem; // Returning the created object
    }

    @Override
    public void onBindViewHolder(myAdapter.ViewHolder holder, int position) {
        holder.titles.setText(mTitle[position]);

    }

    @Override
    public int getItemCount() {
        return mTitle.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

      public TextView titles;
        public ImageView icons;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            this.titles = (TextView) itemView.findViewById(R.id.rowText);

        }
    }

}