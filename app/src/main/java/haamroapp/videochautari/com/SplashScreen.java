package haamroapp.videochautari.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import haamroapp.videochautari.com.util.DownloadUtil;
import haamroapp.videochautari.com.util.NetworkUtil;

public class SplashScreen extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 50000;

    ProgressBar bar;
    private int nSdkVersion, nSdkVers;
    String versionName = "";
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Downloading the update");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);

        Thread thread = new Thread() {
            public void run() {
                try {

                    sleep(3000);

                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } finally {

                    if (NetworkUtil.isInternetAvailable(SplashScreen.this)) {

                        try {
                            PackageInfo pInfo = getPackageManager().getPackageInfo(
                                    getPackageName(), 0);
                            versionName = pInfo.versionName;
                            nSdkVersion = Integer.parseInt(Build.VERSION.SDK); // 19
                            nSdkVers = Build.VERSION.SDK_INT; // 19


                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                        Log.e("App Version", "" + versionName);

                        String linkVersionCheck = "http://worldondemand.net/videochautari_version.php?version="
                                + versionName;
                        splash();
//                        new JSONParserVersionCheck(SplashScreen.this).execute(linkVersionCheck);

                    }

                }

            }


        };
        thread.start();
    }





    /* Auto Update Code */
    public class JSONParserVersionCheck extends
            AsyncTask<String, String, String> {

        private String version;
        private Activity activity=null;
        public JSONParserVersionCheck(Activity activity) {
            this.activity = activity;
        }


        @Override
        protected String doInBackground(String... params) {
            Log.d("EntryPoint", params[0]);
            DownloadUtil dUtil = new DownloadUtil(params[0], SplashScreen.this);
            return dUtil.downloadStringContent();

      }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            try {

                JSONObject jObj = new JSONObject(result);
                boolean updateRequired = jObj.getBoolean("updateRequired");

                if (updateRequired) {
                    final String updateLink = jObj.getString("updateLocation");
                    Log.d("link", updateLink);

                    AlertDialog dialog = new AlertDialog.Builder(this.activity)
                            .setPositiveButton("UPDATE",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {


                                            APKDownloader installer = new APKDownloader(
                                                    activity);
                                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                                                installer.execute(updateLink);
                                            } else {
                                                installer
                                                        .executeOnExecutor(
                                                                AsyncTask.THREAD_POOL_EXECUTOR,
                                                                updateLink);
                                            }
//

                                        }
                                    })
                            .setMessage(getString(R.string.update_message))
                            .create();

                    dialog.show();


                }
                else {
                    splash();
                }
            } catch (JSONException e) {
                // Log.e("JSON Parser", "Error parsing data " + e.toString());
                splash();
            }
        }
    }


    public void splash(){
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {



                int screenSize = getResources().getConfiguration().screenLayout
                        & Configuration.SCREENLAYOUT_SIZE_MASK;
                Intent mainIntent;

                switch (screenSize) {
                    case Configuration.SCREENLAYOUT_SIZE_LARGE:
//                        mainIntent = new Intent(SplashScreen.this, VideoChautari.class);
//
//                        SplashScreen.this.startActivity(mainIntent);
//                        SplashScreen.this.finish();
                        break;
                    case Configuration.SCREENLAYOUT_SIZE_NORMAL:


                        mainIntent = new Intent(SplashScreen.this, MobileVideoChautari.class);
                        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        SplashScreen.this.startActivity(mainIntent);
                        SplashScreen.this.finish();

                        break;
                    case Configuration.SCREENLAYOUT_SIZE_SMALL:


                        mainIntent = new Intent(SplashScreen.this, MobileVideoChautari.class);
                        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        SplashScreen.this.startActivity(mainIntent);
                        SplashScreen.this.finish();

                    default:
//                        mainIntent = new Intent(SplashScreen.this, VideoChautari.class);
//                        SplashScreen.this.startActivity(mainIntent);
//                        SplashScreen.this.finish();
                }



    }

    public class APKDownloader extends AsyncTask<String, Integer, File> {

        InputStream is = null;
        Activity activity = null;
        private PowerManager.WakeLock mWakeLock;

        public APKDownloader(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected File doInBackground(String... params) {
            // TODO Auto-generated method stub
            File file = null;

            try {
                // set the download URL, a url that points to a file on the
                // internet
                // this is the file to be downloaded
                String updateLink = params[0];

                URL url = new URL(updateLink);
                Log.d(updateLink, updateLink);

                // create the new connection
                HttpURLConnection urlConnection = (HttpURLConnection) url
                        .openConnection();

                // set up some things on the connection
                urlConnection.setRequestMethod("GET");

                //if(!updateLink.toLowerCase().startsWith("https"))
                //urlConnection.setDoOutput(true);

                // and connect!
                urlConnection.connect();

                File SDCardRoot = Environment.getExternalStorageDirectory();
                String fileName = updateLink.substring(updateLink
                        .lastIndexOf("/"));

                Log.d(fileName, fileName);
                file = new File(SDCardRoot, fileName);

                // this will be used to write the downloaded data into the file
                // we created
                FileOutputStream fileOutput = new FileOutputStream(file);

                // this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                // this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                Log.d(totalSize+"", totalSize+"");
                // variable to store total downloaded bytes
                int downloadedSize = 0;

                // create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0; // used to store a temporary size of the
                // buffer

                // now, read through the input buffer and write the contents to
                // the file
                Log.d("before loop", "let us see");
                Log.d(bufferLength+"", inputStream.read(buffer)+"");


                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    Log.e("loop", inputStream.read(buffer)+"");

                    // add the data in the buffer to the file in the file output
                    // stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    // add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    // Log.d( "com.redorange.motutv1.EntryPoint", downloadedSize
                    // + "" );

                    // this is where you would do something to report the
                    // prgress, like this maybe
                    // updateProgress(downloadedSize, totalSize);

                    if (totalSize > 0) {
                        publishProgress((downloadedSize * 100 / totalSize));
                    }


                }
                // close the output stream when done
                fileOutput.close();
            } catch (Exception e) {
                e.printStackTrace();
                 Log.e("Buffer Error", "Error converting result " +
                 e.toString());

                AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setPositiveButton("OK", null)
                        .setMessage("Error: " + e.toString()).create();

                dialog.show();
            }

            return file;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            PowerManager pm = (PowerManager) activity.getApplicationContext()
                    .getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(values[0]);
            Log.d("slfjsa",		values[0].toString());
        }



        public int getScreenOrientation()
        {
            Display getOrient = getWindowManager().getDefaultDisplay();
            int orientation = Configuration.ORIENTATION_UNDEFINED;
            if(getOrient.getWidth()==getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_SQUARE;
            } else{
                if(getOrient.getWidth() < getOrient.getHeight()){
                    orientation = Configuration.ORIENTATION_PORTRAIT;
                }else {
                    orientation = Configuration.ORIENTATION_LANDSCAPE;
                }
            }
            return orientation;
        }
        @Override
        protected void onPostExecute(File savedFile) {
            super.onPostExecute(savedFile);

            mWakeLock.release();
            mProgressDialog.dismiss();

            Log.d("com.redorange.motutv1.EntryPoint", "Saved File at: " +
                    savedFile.toString());
            if (savedFile.exists()) {
                try {
					/*
					 * Intent promptInstall = new Intent(Intent.ACTION_VIEW);
					 * promptInstall.setDataAndType(Uri.parse(
					 * savedFile.toString() ),
					 * "application/vnd.android.package-archive");
					 * promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					 */

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(savedFile),
                            "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    activity.getApplicationContext().startActivity(intent);
                    getParent().finish();

                } catch (Exception e) {
					/*
					 * AlertDialog dialog = new AlertDialog.Builder( activity )
					 * .setPositiveButton("OK", null) .setIcon(R.drawable.logo)
					 * .setMessage( "Install Error: " + e.toString() )
					 * .create();
					 *
					 * dialog.show();
					 */
                }
            }
        }

    }




    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

}
