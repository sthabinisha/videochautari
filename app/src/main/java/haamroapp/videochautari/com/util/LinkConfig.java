package haamroapp.videochautari.com.util;

import android.content.Context;

import haamroapp.videochautari.com.R;

/**
 * Created by binisha on 9/8/15.
 */
public class LinkConfig {
    public static String VERSION_URL = "http://worldondemand.net/";
    public static String BASE_URL_DuringDevelopment = VERSION_URL;
    public static final int VERSION = R.string.version;

    public static String getString(Context context, int resId) {
        //CHECK_IF_SERVER_RECHABLE = "192.168.0.115";
        return BASE_URL_DuringDevelopment
                + context.getResources().getString(resId);

    }



}
