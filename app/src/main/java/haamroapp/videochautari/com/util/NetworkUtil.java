package haamroapp.videochautari.com.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import haamroapp.videochautari.com.NoInternetActivity;
import haamroapp.videochautari.com.R;


public class NetworkUtil {

    public static final int NETWORK_STATE_UNKNOWN = 1000;
    public static final int NETWORK_NOT_AVAILABLE = 1001;
    public static final int SERVER_HOST_NOT_AVAILABLE = 1002;

    public static final int NETWORK_STATE_UNKNOWN_MESSAGE = R.string.internet_not_available;
    public static final int NETWORK_NOT_AVAILABLE_MESSAGE = R.string.internet_not_available;
    public static final int SERVER_HOST_NOT_AVAILABLE_MESSAGE = R.string.server_not_available;

    private static final String TAG = "NetworkUtil";

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        boolean isConnected3G = false;

        if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null) {
            isConnected3G = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        }
        boolean isConnectedWifi = false;

        if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null) {
            isConnectedWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        }

        //if (netInfo != null && netInfo.isConnectedOrConnecting()) {
        if (netInfo != null && (isConnected3G || isConnectedWifi)) {
            return true;
        }
        return false;
    }

    // To check if server is reachable
    public static boolean isServerReachable() {
        // this method cannot be run on main thread
        // currently looking for alternative solution to detect if server is reachable
        /*
		try {
			InetAddress.getByName("http://www.nepalchannels.com").isReachable(50000); // Replace
		
			Logger.d(TAG, "connection established");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.d(TAG, "connection lost");
			return false;
		}*/

        return true;
    }

    public static boolean isInternetAvailable(Context context) {

        if (isOnline(context)) {
            return true;
        } else {

            int cause = NetworkUtil.NETWORK_STATE_UNKNOWN;
            int message = NetworkUtil.NETWORK_STATE_UNKNOWN_MESSAGE;

            if (!isOnline(context)) {
                cause = NetworkUtil.NETWORK_NOT_AVAILABLE;
                message = NetworkUtil.NETWORK_NOT_AVAILABLE_MESSAGE;
            } else {
                cause = NetworkUtil.SERVER_HOST_NOT_AVAILABLE;
                message = NetworkUtil.SERVER_HOST_NOT_AVAILABLE_MESSAGE;
            }

            Intent intent = new Intent(context, NoInternetActivity.class);

            intent.putExtra(NoInternetActivity.INTERNET_UNAVAILABLE, cause);
            intent.putExtra(NoInternetActivity.INTERNET_UNAVAILABLE_MESSAGE, message);

            context.startActivity(intent);
            //finish();

            return false;
        }
    }
}
