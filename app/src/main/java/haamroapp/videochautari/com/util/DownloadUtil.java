
package haamroapp.videochautari.com.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.util.List;

import haamroapp.videochautari.com.NoInternetActivity;


public class DownloadUtil {

	public static String NotOnline = "1";
	public static String ServerUnrechable = "0";
	private static final String TAG = "com.newitventure.smartvision.movies.util.DownloadUtil";

	private String link;
	private Context context;
	private String encoding = "utf-8";

	public DownloadUtil(String link, Context context) {
		this.link = link;
		this.context = context;
	}

	public String downloadStringContent() {
		if (isOnline()) {

				String responseString = "";

				try {
					DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpPost httpGet = new HttpPost(link);

					// BasicResponseHandler responseHandler = new
					// BasicResponseHandler();
					// String json = httpClient.execute(httpGet,
					// responseHandler);

					HttpResponse httpResponse = httpClient.execute(httpGet);
					HttpEntity httpEntity = httpResponse.getEntity();

					responseString = EntityUtils.toString(httpEntity);

					// Log.i(TAG, json + "------" );
				} catch (UnsupportedEncodingException e) {

					Log.e(TAG, "UnsupportedEncodingException: " + e.toString());
					return ServerUnrechable;

				} catch (ClientProtocolException e) {

					Log.e(TAG, "ClientProtocolException: " + e.toString());
					return ServerUnrechable;

				} catch (IOException e) {

					Log.e(TAG, "IOException: " + e.toString());
					return ServerUnrechable;

				} catch (Exception e) {

					Log.e(TAG, "Exception: " + e.toString());
					return ServerUnrechable;

				}

				return responseString;

		} else
			return NotOnline;

	}



	private boolean isServerReachable(String value)
	// To check if server is reachable
	{
		try {
			InetAddress.getByName(value).isReachable(3000); // Replace
			// with
			// your
			// name
			// InetAddress.getByName("asdksjdjkshdd.askjdhasjk.sakd").isReachable(3000);
			// //Replace with your name
			Log.d(TAG, "connection established");
			return true;
		} catch (Exception e) {
			Log.d(TAG, "connection lost");
			return false;
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}



	public String postDataAndGetStringResponse( List<NameValuePair> nameValuePairs ) {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost( link );

		try {

			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = client.execute(post);

			InputStream is = response.getEntity().getContent();

			String result = "";

			if (is != null) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));

				String l = "";
				while ((l = reader.readLine()) != null) {
					result += l;
				}

				reader.close();
			}

			is.close();

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static boolean checkdownloadStringContent(Context context,
													 String result) {
		if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
			Log.d(TAG, "internet not available");
			Intent intent = new Intent(context, NoInternetActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			((Activity) context).finish();
			return false;
			// server not rechable
		}  else {
			return true;
		}
	}
}
